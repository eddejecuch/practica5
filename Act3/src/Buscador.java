/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eduardo de Jesuc Cuellar Chavez
 */
public class Buscador<E extends Comparable<E>> {

    private E elementos[];

    public Buscador(E elementos[]) {
        this.elementos = elementos;
    }

    public int hacerBusquedaSecuencial(E elementoBuscado) {
        int posicion = 0;
        for (int i = 0; i < elementos.length; i++) {
            if (elementos[i].compareTo(elementoBuscado) == 0) {
                posicion = i;
                break;
            }
        }
        return posicion;
    }

    public int hacerBusquedaBinaria(E elementoBuscado) {
        int inicio = 0;
        int posicion = 0;
        int fin = elementos.length - 1;
        int mitad = (inicio + fin) / 2;
        while (inicio <= fin) {
            if (elementos[mitad].compareTo(elementoBuscado) == -1) {
                inicio = mitad + 1;
            } else if (elementos[mitad].compareTo(elementoBuscado) == 0) {
                posicion = mitad;
                break;
            } else {
                fin = mitad - 1;
            }
            mitad = (inicio + fin) / 2;
        }
        return posicion;
    }
}
